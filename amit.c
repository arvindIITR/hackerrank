#include <graphics.h>
#include <conio.h>

main()
{
   int gd = DETECT, gm;

   initgraph(&gd, &gm, "C:\\TC\\BGI");

   line(100,100,200,100);
   arc(100, 100, 0, 90, 50);
   arc(100, 100, 0, -90, 50);
   line(110,100,110,200);
   arc(110, 200, 0, -180, 50);
   line(130,100,130,200);
   line(130,100,120,120);
   line(120,120,140,120);
   line(140,100,140,200);
   line(160,100,160,200);
   line(160,150,150,150);
   arc(160,150,180,220,30);

   getch();
   closegraph();
   return 0;
}
