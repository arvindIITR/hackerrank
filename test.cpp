#include <iostream>
#include <string>

using namespace std;
// returns count of non-overlapping occurrences of 'sub' in 'str'
int countSubstring(const std::string& str, const std::string& sub)
{
    if (sub.length() == 0) return 0;
    if(str.length() == 0) return sub.length();
    int count = 0;
    for (size_t offset = str.find(sub); offset != std::string::npos;
	 offset = str.find(sub, offset + sub.length()))
    {
        ++count;
    }
    return count;
}

int main()
{
    char str[10000], txt[10000];
    cin>>str>>txt;
    cout << countSubstring(txt,str)    << '\n';
    return 0;
}
