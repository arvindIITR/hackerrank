#include <iostream>
#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<cstring>

using namespace std;

long long ans;
int cnt[26]={0};

long factorial(long n)
 {
    if(n ==0 || n==1)
        return 1;
     return n*factorial(n-1);
}

int main()
{
    char s[100010];
    scanf("%s",s);
    int len = strlen(s);
    for(int i=0;i<len;i++)
        cnt[s[i] - 'a']++;

    //int even=0, odd =0;

    long   sum =0;
    long   prod =1;

    //for(int i=0;i<26;i++)
       // sum += floor((int)cnt[i]/2);
    sum = ( factorial((int)floor(len/2))) / 1000000007;
    for(int i=0; i< 26; i++)
        prod *= (factorial((long) floor((int)cnt[i]/2)) ) / 1000000007;
    ans = (sum/prod);
    cout<<ans<<endl;
    return 0;
}


