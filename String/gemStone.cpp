#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<string>
#include <map>
#include <vector>


using namespace std;


void remove_dup(string& gems) {
    map<char, bool> visited;
    int len = gems.length();
    int i = 0;
    while(i < len) {
        if(visited[gems[i]]) {
            gems.erase(i, 1);
            len--;
            continue;
        }
        visited[gems[i]] = true;
        ++i;
    }
}
int main()
{
    int n;
    scanf("%d",&n);
    string s[n];
    int arr[26] = {0};
    for(int i=0;i<n;i++)
        cin>>s[i];

    for(int i=0;i<n;i++)
        remove_dup(s[i]);
    int res=0;
    for(int i=0;i<n;i++)
    {
       for(int j=0;j<s[i].length();j++)
            arr[(s[i][j] -'a')]++;
    }
    for(int i=0;i<26;i++)
    {
       if(arr[i] == n)
          res++;
    }

    cout<<res<<endl;

    return 0;
}
