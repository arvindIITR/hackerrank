#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include<cstring>
using namespace std;


int main() {
    int t;
    scanf("%d",&t);
    char str[105];
    int arr[101];
    while(t--)
    {
        scanf("%s",str);
        int len = strlen(str);
        for(int i=0;i<len;i++)
            arr[i] = str[i]-'a';
        if(next_permutation(arr,arr+len))
        {
          for(int i=0;i<len;i++)
              printf("%c",arr[i]+'a');
             printf("\n");
        }
        else
            printf("no answer\n");

    }
    return 0;
}
