/* Algorithm

  1. Place the starting node s on the top of the stack.
  2. If the stack is empty, return failure and stop.
  3. If the element on the stack is goal node g, return success and stop. Otherwise,
  4. Remove and expand the first element , and place the children at the top of the stack.
  5. Return to step 2.
*/

#include<iostream>
#include<ctime>
#include<malloc.h>
#include<stack>

using namespace std;

struct node
{
    string name;
    int cost;
    int TCost;
    node *next;
};

class graph
{
    private:
        int n;
        int **A;
    public:
        graph(int size = 2);
        ~graph();
        bool isConnected(int, int);
        void addEdge(int x, int y);
        void DFS(int , int);
};
graph :: graph(int size)
{
    //int i,j;
    if(size < 2) n=2;
    else n=size;
    A = new int* [n];
    for(int i=0; i<n; i++)
        A[i] = new int[n];
    for(int i=0;i<n;i++)
        for(int j=0;j<n;j++)
          A[i][j] = 0;
}

graph :: ~graph()
{
    for(int i=0; i<n; i++)
        delete [] A[i];
    delete []A;
}
 bool graph::isConnected(int x, int y)
 {
     return (A[x-1][y-1] == 1);
 }
 void graph :: addEdge(int x, int y)
 {
     A[x-1][y-1] = A[y-1][x-1] = 1;
 }

 void graph :: DFS(int x, int req)
 {
     stack<int> s;

     bool *vis = new bool[n+1];
     for(int i=0; i<=n; i++)
        vis[i] = false;
     s.push(x);
     vis[x] = true;
     if(x == req) return;
     cout<<"Depth first Search starting from vertex";
     cout<<x<<" : "<<endl;
     int count=0;
     while(!s.empty())
     {
         int k ;
         k= s.top();
         s.pop();
         if(k == req)
         {   cout<<k<<endl;
             cout<<count;
             break;
         }

         cout<<k<<" ";
         for(int i=n; i>=0; --i)
             if(isConnected(k, i) && !vis[i])
               {
                  s.push(i);
                  vis[i] = true;

                  count++;
               }
     }
     cout<<endl;
     delete [] vis;
 }

 int main()
 {
     graph g(8);
     g.addEdge(1,2);
     g.addEdge(1,3);
     g.addEdge(1,4);
     g.addEdge(2,5);
     g.addEdge(2,6);
     g.addEdge(4,7);
     g.addEdge(4,8);
     g.DFS(1,4);

     return 0;
 }
