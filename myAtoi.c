

 #include<stdio.h>
 #include<limits.h>
 #include<stdbool.h>

 #define MAX INT_MAX


bool isNumericChar(char x)
{
    return (x >= '0' && x <= '9')? true: false;
}


int myAtoi(char *str)
{
    if (*str == NULL)
       return 0;

    int res = 0;
    int sign = 1;  // Initialize sign as positive
    int i = 0;  // Initialize index of first digit

    // If number is negative, then update sign
    if (str[0] == '-')
    {
        sign = -1;
        i++;
    }


    for (; str[i] != '\0'; ++i)
    {
        if (isNumericChar(str[i]) == false)
            return 0;  // if invalid character return 0
        res = res*10 + str[i] - '0';
    }

    // Return result with sign
    return sign*res;
}

int main()
{
     char str[1000];
     scanf("%s",str);
     int res = myAtoi(str);
     printf("%d", res);
     return 0;
}
